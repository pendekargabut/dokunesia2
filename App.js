import React from "react";
import {
  Alert,
  StyleSheet,
  Text,
  View,
  Button,
  FlatList,
  TouchableOpacity,
  ToastAndroid,
  AsyncStorage,
  ScrollView,
  Image,
  ImageBackground,
  Dimensions
} from "react-native";
import { SearchBar, Card, Input, Icon } from "react-native-elements";
import { LinearGradient, FileSystem, Constants } from "expo";
import shorthash from "shorthash";
import { Ionicons } from "@expo/vector-icons";
//var RNFetchBlob = require('rn-fetch-blob').default
import {
  createStackNavigator,
  createAppContainer,
  createSwitchNavigator,
  createBottomTabNavigator,
  createDrawerNavigator
} from "react-navigation";


// Main Components
import SplashScreen from "./components/Splash";
import HomeScreen from "./components/Home";
import AboutScreen from "./components/About";
import BookmarkScreen from "./components/Bookmark";
import SearchScreen from "./components/Search";
import SearchResultScreen from "./components/SearchResult";
import ProfileScreen from "./components/Profile";

//Sub Components
import IconWithBadge from "./components/sub_components/IconWithBadge";
import CustomHeaderBackImage from "./components/sub_components/CustomHeaderBackImage";

const HomeIconWithBadge = props => {
  // You should pass down the badgeCount in some other ways like context, redux, mobx or event emitters.
  return <IconWithBadge {...props} />;
};

export const HomeStackNavigator = createStackNavigator(
  {
    HomeScreen: { screen: HomeScreen },
    SearchScreen: { screen: SearchScreen },
    SearchResultScreen: { screen: SearchResultScreen },
    ProfileScreen: { screen: ProfileScreen },
  },
  {
    initialRouteName: "HomeScreen",
    defaultNavigationOptions: {
      headerBackImage: (
        <CustomHeaderBackImage />
      ),
      headerStyle: {
        backgroundColor: "#6DCBD9"
      },
      headerTitleStyle: {
        fontSize: 25,
        color: "#fff"
      }
    }
  }
);

export const MainTabNavigator = createBottomTabNavigator(
  {
    Home: HomeStackNavigator,
    Bookmark: { screen: BookmarkScreen },
    About: { screen: AboutScreen }
  },
  {
    initialRouteName: "Home",
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let IconComponent = Ionicons;
        let iconName;
        if (routeName === "Home") {
          iconName = `ios-home`;
          // Sometimes we want to add badges to some icons.
          // You can check the implementation below.
          IconComponent = HomeIconWithBadge;

          // return (
          //   <Image
          //     source={require("./assets/images/ElementMenu/01BS-Home.png")}
          //     style={{ height: 24, width: 24 }}
          //   />
          // );
        } else if (routeName === "Bookmark") {
          iconName = `ios-bookmark`;
        } else if (routeName === "About") {
          iconName = `ios-information-circle`;
        }

        // You can return any component that you like here!
        return <IconComponent name={iconName} size={25} color={tintColor} />;
      }
    }),
    tabBarOptions: {
      activeTintColor: "#7FC891",
      inactiveTintColor: "gray"
    }
  }
);

const AppContainer = createAppContainer(MainTabNavigator);

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { currentScreen: "Splash" };

    setTimeout(() => {
      this.setState({ currentScreen: "Home" });
    }, 3000);
  }
  render() {
    const { currentScreen } = this.state;
    let mainScreen =
      currentScreen == "Splash" ? <SplashScreen /> : <AppContainer />;
    return mainScreen;
  }
}