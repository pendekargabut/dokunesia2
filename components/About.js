import React from "react";
import { StyleSheet, Text, View, Image, ScrollView, Dimensions } from "react-native";
import { LinearGradient } from "expo";
import styles from "../styles/GlobalStyles"

export default class AboutScreen extends React.Component {
  
  render() {
    return (
      <ScrollView>
        <View style={styles.halfScreenContainer}>
          <LinearGradient
            colors={["#7FC891", "#6DCBD9"]}
            style={styles.container}
          >
            <Image
              source={require("../assets/images/ElementMenu/09About.png")}
              style={styles.topHalfImageStyle}
              resizeMode="cover"
            />
            <View style={styles.topHalfTextContainer}>
              <Text style={styles.titleText}>About</Text>
              <Text style={styles.subTitleText}>
                Learning About What We Do.
              </Text>
            </View>
          </LinearGradient>
        </View>
        <View style={styles.aboutContentContainer}>
          <View style={styles.aboutContentWrapper}>
            <View style={styles.aboutContentLeftText}>
              <Text style={styles.aboutContentTitleTextStyle}>
                Keep Your Degrees Safe & Flexible
              </Text>
              <Text>
                Lorem ipsum is simply a dummy text of the printing and
                typesetting industry. Lorem ipsum has been industry's standard
                dummy text ever since the 1500s.
              </Text>
            </View>
            <View style={{ flex: 0.45 }}>
              <Image
                source={require("../assets/images/ElementMenu/09About.png")}
                style={styles.aboutContentImage}
                resizeMode="contain"
              ></Image>
            </View>
          </View>
          <View style={styles.aboutContentWrapper}>
            <View style={{ flex: 0.45 }}>
              <Image
                source={require("../assets/images/ElementMenu/09About.png")}
                style={styles.aboutContentImage}
                resizeMode="contain"
              ></Image>
            </View>
            <View style={styles.aboutContentRightText}>
              <Text style={styles.aboutContentTitleTextStyle}>
                Find New Employee
              </Text>
              <Text>
                it is a long established fact that a reader will be distracted
                by the readable content of a page when looking at its layout.
                The point of using Lorem Ipsum is that it has a more-or-less
                normal distribution of letters, as opposed to using 'Content
                here, content here', making it more readable than before.
              </Text>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}
