import React from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  ToastAndroid,
  AsyncStorage,
  Dimensions
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import Carousel from "./sub_components/Carousel";
import CacheImage from "./sub_components/CacheImage";
import DefaultHeaderNavigationBar from "./sub_components/DefaultHeaderNavigationBar";
import styles from "../styles/GlobalStyles";
import ImageView from "react-native-image-view";

const screenWidth = Math.round(Dimensions.get("window").width);

export default class ProfileScreen extends React.Component {
  state = {};

  static navigationOptions = ({ navigation }) => {
    const title = navigation.getParam("itemName", "Item Name");
    return {
      title: { title },
      header: (
        <DefaultHeaderNavigationBar navigation={navigation} title={title} />
      )
    };
  };

  renderSeparator = () => (
    <View
      style={{
        backgroundColor: "#D3D3D3",
        height: 2,
        marginBottom: 20
      }}
    />
  );

  componentDidMount = async () => {
    const { navigation } = this.props;
    var a = [];
    var index = -1;
    var count = 0;
    // return fetch('192.168.1.80:9191/api/document/search/a')
    const itemId = navigation.getParam("itempid");
    // console.log(itemId);
    //console.log("yang di search : "+itemId);

    console.log("call save to recent function");
    const newRecentItem = {
      personID: navigation.getParam("itempid"),
      name: navigation.getParam("itemName"),
      lastUniversity: navigation.getParam("itemUniversity")
    };

    this.saveToRecent(newRecentItem);

    return fetch("http://192.168.1.80:9191/api/blob/metadata/" + itemId, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => response.json())
      .then(responseJson => {
        //  console.log("responseJson")
        //console.log(responseJson)

        var temp = [];

        for (let i = 0; i < responseJson.length; i++) {
          if (a[responseJson[i].degree] === undefined) {
            //console.log("kosong")
            // console.log(responseJson[i].degree)
            index = index + 1;
            // count=0;
            a[responseJson[i].degree] = responseJson[i];

            temp[index] = [
              "http://192.168.1.80:9191/api/blob/" + responseJson[i].files
            ];

            //   const name = shorthash.unique('http://192.168.1.80:9191/api/blob/'+responseJson[i].files);
            //       //console.log(name);
            //       const path = `${FileSystem.cacheDirectory}${name}`;
            //       const image = FileSystem.getInfoAsync(path);
            //       if (image.exists) {

            //       }
            //       else {
            // const newImage = FileSystem.downloadAsync('http://192.168.1.80:9191/api/blob/'+responseJson[i].files, path)
            //       }

            console.log(
              "http://192.168.1.80:9191/api/blob/" + responseJson[i].files
            );
          } else {
            //count=count+1
            var templagi = temp[index][0];

            temp[index] = [
              templagi,
              "http://192.168.1.80:9191/api/blob/" + responseJson[i].files
            ];
            // const name = shorthash.unique('http://192.168.1.80:9191/api/blob/'+responseJson[i].files);
            //       //console.log(name);
            //       const path = `${FileSystem.cacheDirectory}${name}`;
            //       const image = FileSystem.getInfoAsync(path);
            //       if (image.exists) {

            //       }
            //       else {
            // const newImage = FileSystem.downloadAsync('http://192.168.1.80:9191/api/blob/'+responseJson[i].files, path)
            //       }

            console.log(
              "http://192.168.1.80:9191/api/blob/" + responseJson[i].files
            );
          }

          // else
          // a[responseJson[i].degree].push(responseJson[i])
          //console.log(a[responseJson[i].degree])
        }
        // console.log(temp.length)
        var deleteIndex = 1;
        for (let i = 0; i < temp.length; i++) {
          responseJson[i].filesemua = temp[i];
          var deleteUntil = temp[i].length - 1;
          responseJson.splice(deleteIndex, deleteUntil);
          deleteIndex = deleteIndex + 1;
          //console.log(responseJson)
          //console.log(responseJson)
        }

        //console.log(responseJson)
        //console.log(a)
        //a=[a["S1"],a["S2"]]
        //console.log(a.length)
        this.setState(
          {
            // isLoading: false,
            dataSource: responseJson
          },
          function () { }
        );
      })

      .catch(function (error) {
        console.log(
          "There has been a problem with your fetch operation: " + error.message
        );
        // ADD THIS THROW error
        throw error;
      });
    // fetch(

    // )
    // .then((response) =>{
    //   console.log(response);
    //   return response.json();
    // })
    // .then((resParsed) => {
    //  console.log(resParsed);
    // })
  };
  
  saveToRecent = newRecentItem => {
    console.log("Passed Parameter");
    console.log(newRecentItem);

    console.log("calling async storage");
    AsyncStorage.getItem("recent").then(value => {
      console.log("Async storage called");
      const recentList = JSON.parse(value) ? JSON.parse(value) : [];
      console.log(recentList);
      console.log(recentList.length);

      if (recentList.length === 0) {
        recentList.push(newRecentItem);
      } else if (
        recentList.length > 0 &&
        recentList.findIndex(
          recent => recent.personID === newRecentItem.personID
        ) == -1
      ) {
        console.log("else called");
        if (recentList.length == 4) {
          recentList.splice(0, 1);
        }

        recentList.push(newRecentItem);
      }

      console.log(recentList);
      AsyncStorage.setItem("recent", JSON.stringify(recentList));
    });
  };


  render() {
    const { navigation } = this.props;
    const itemId = navigation.getParam("itemName", "NO-ID");
    const itemDetail = navigation.getParam("itemDetail", "NO-ID");
    const api = "http://192.168.1.80:9191/api/blob/";
    // const pid = navigation.getParam('itempid');
    const itemmeta = this.state.dataSource;
    // console.log("isi item meta ");
    // console.log(itemmeta);
    //  console.log(pid)
    return (
      <View>
        <FlatList
          ItemSeparatorComponent={this.renderSeparator}
          style={{ paddingLeft: 8, paddingRight: 8 }}
          data={this.state.dataSource}
          renderItem={({ item }) => (
            <View style={{ marginBottom: 30 }}>
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: "bold",
                  paddingTop: 10,
                  paddingBottom: 20
                }}
              >
                {item.year}-{item.university}
              </Text>

              <CacheImage
                uri={item.filesemua}
                length={item.filesemua.length}
                style={{
                  width: screenWidth - 16,
                  height: 300,
                  marginBottom: 30
                }
                } navigation={navigation}
              />
              <View style={{ flexDirection: "row" }}>
                <Text style={{ minWidth: 120 }}>Nama </Text>
                <Text>: {itemId}</Text>
              </View>
              <View style={{ flexDirection: "row" }}>
                <Text style={{ minWidth: 120 }}>DocumentType </Text>
                <Text>: {item.documentType}</Text>
              </View>
              <View style={{ flexDirection: "row" }}>
                <Text style={{ minWidth: 120 }}>Year </Text>
                <Text>: {item.year}</Text>
              </View>
            </View>
          )}
        />
        
      </View>
    );
  }

  saveData(itemId, itemDetail) {
    AsyncStorage.getItem("user").then(value => {
      if (value) {
        value = JSON.parse(value) + ";" + itemId + ":" + itemDetail;
        AsyncStorage.setItem("user", JSON.stringify(value));
        // AsyncStorage.removeItem('user');
        ToastAndroid.show(value, ToastAndroid.SHORT);
      } else {
        AsyncStorage.setItem("user", JSON.stringify(itemId + ":" + itemDetail));
        ToastAndroid.show("null", ToastAndroid.SHORT);
      }
    });

    //AsyncStorage.removeItem('user');
    //let value = await AsyncStorage.getItem('user');
  }
}
