import React, { version } from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  AsyncStorage
} from "react-native";
import { Input } from "react-native-elements";
import { Ionicons } from "@expo/vector-icons";
import SearchHeaderNavigationBar from "./sub_components/SearchHeaderNavigationBar";
import styles from "../styles/GlobalStyles";
import SearchRecent from "./SearchRecent";
import SearchResult from "./SearchResult";
import SearchSuggestion from "./SearchSuggestion";
import { withNavigation } from "react-navigation";

class SearchScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      onSearchText: "",
      onSubmitSearchText: "",
      recentList: [],
      searchState: "recent",
      suggestionList: [],
      resultList: [],
      bookmarkList: []
    };

    this.updateOnChangeText = this.updateOnChangeText.bind(this);
    this.changeSearchState = this.changeSearchState.bind(this);
    this.updateRecentState = this.updateRecentState.bind(this);
    this.updateSuggestionState = this.updateSuggestionState.bind(this);
    this.updateResultState = this.updateResultState.bind(this);
    this.fetchResultData = this.fetchResultData.bind(this);
    this.checkBookmarkData = this.checkBookmarkData.bind(this);
    this.onSubmitState = this.onSubmitState.bind(this);
    this.onSuggestionChange = this.onSuggestionChange.bind(this);
    this.fetchSuggestionData = this.fetchSuggestionData.bind(this);
    this.setResultState = this.setResultState.bind(this);
    this.fetchBookmarkList = this.fetchBookmarkList.bind(this);
    this.updateBookmarkList = this.updateBookmarkList.bind(this);
    this.onRecentState = this.onRecentState.bind(this);
  }

  componentDidMount() {
    const { navigation } = this.props;

    this.props.navigation.setParams({
      changeSearchState: this.changeSearchState,
      updateOnChangeText: this.updateOnChangeText,
      fetchResultData: this.fetchResultData,
      onSubmitState: this.onSubmitState,
      onSuggestionChange: this.onSuggestionChange,
      onRecentState: this.onRecentState
    });
    this.setState({
      onSearchText: "",
      onSubmitSearchText: "",
      recentList: [],
      searchState: "recent",
      suggestionList: [],
      resultList: [],
      bookmarkList: []
    });
    this.focusListener = navigation.addListener("didFocus", () => {
      if (this.state.searchState == "recent") {
        this.onRecentState("recent");
      } else if (this.state.searchState == "suggestion") {
        this.updateSuggestionState(this.state.suggestionList);
      } else if (this.state.searchState == "result") {
        this.setState({
          searchState: "result",
          onSubmitSearchText: this.state.onSubmitSearchText
        });
      }
    });
  }

  componentWillUnmount() {
    this.focusListener.remove();
  }

  static navigationOptions = ({ navigation }) => {
    // this is included because you had added it and you may require the params in your component
    const { params } = navigation.state;

    return {
      title: "Search",
      header: (
        <SearchHeaderNavigationBar
          {...params}
          navigation={navigation}
          title={"Search"}
          changeSearchState={navigation.getParam("changeSearchState")}
          onSubmitState={navigation.getParam("onSubmitState")}
          updateOnChangeText={navigation.getParam("updateOnChangeText")}
          fetchResultData={navigation.getParam("fetchResultData")}
          onSuggestionChange={navigation.getParam("onSuggestionChange")}
          onRecentState={navigation.getParam("onRecentState")}
        />
      )
    };
  };

  renderSeparator = () => (
    <View
      style={{
        backgroundColor: "black",
        height: 1
      }}
    />
  );

  onRecentState(searchState) {
    AsyncStorage.getItem("recent").then(value => {
      this.setState({
        searchState: searchState,
        recentList: JSON.parse(value) ? JSON.parse(value) : []
      });
    });
  }

  changeSearchState(searchState) {
    this.setState({
      searchState: searchState
    });
  }

  updateRecentState() {
    AsyncStorage.getItem("recent").then(value => {
      this.setState({
        recentList: JSON.parse(value) ? JSON.parse(value) : []
      });
    });
  }

  updateSuggestionState(suggestionList) {
    this.setState({
      suggestionList: suggestionList
    });
  }

  fetchBookmarkList() {
    AsyncStorage.getItem("bookmark").then(value => {
      this.setState({
        bookmarkList: JSON.parse(value) ? JSON.parse(value) : []
      });
    });
  }

  setResultState(resultList) {
    this.setState({
      resultList: resultList
    });
  }

  updateResultState(result) {
    // this.state.resultList.map((result, i) => {
    //   if (i == index) result.bookmarked = bookmarkStatus;
    //   console.log(result);
    // })
    this.setState({
      resultList: result
    });
    console.log("bookmark status changed");
    console.log(this.state.resultList);
  }

  onSubmitState(submitedText, searchState) {
    // this.fetchResultData(submitedText);
    this.setState({
      onSubmitSearchText: submitedText,
      searchState: searchState
    });
  }

  updateOnChangeText(changedText) {
    this.setState({
      onSearchText: changedText
    });
  }

  fetchResultData(itemData) {
    console.log(itemData);
    this.fetchBookmarkList();
    fetch("http://192.168.1.80:9191/api/document/search/" + itemData, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => response.json())
      .then(responseJson => {
        for (let index = 0; index < responseJson.length; index++) {
          this.state.bookmarkList.map(bookmark => {
            if (bookmark.personID == responseJson[index].personID) {
              responseJson[index].bookmarked = true;
            } else {
              responseJson[index].bookmarked = false;
            }
          });
        }
        this.setResultState(responseJson);
        console.log("Update Result State");
        console.log(this.state.resultList);
      })

      .catch(function(error) {
        console.log(
          "There has been a problem with your fetch operation: " + error.message
        );
        // ADD THIS THROW error
        throw error;
      });

    // return this.state.resultList;
  }

  onSuggestionChange(onSearchText, searchState) {
    this.fetchSuggestionData(onSearchText);
    this.setState({
      onSearchText: onSearchText,
      searchState: searchState
    });
  }

  fetchSuggestionData(itemData) {
    // const itemId = this.props.onSearchText;
    console.log(itemData);
    fetch("http://192.168.1.80:9191/api/document/suggestion/" + itemData, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log(responseJson);
        this.updateSuggestionState(responseJson);
      })

      .catch(function(error) {
        console.log(
          "There has been a problem with your fetch operation: " + error.message
        );
        // ADD THIS THROW error
        throw error;
      });
  }

  checkBookmarkData(itemData) {
    AsyncStorage.getItem("bookmark").then(value => {
      if (value.personID == itemData.personID) {
        return true;
      }
    });
  }

  updateBookmarkList(bookmarks) {
    this.setState({
      bookmarkList: bookmarks
    });
  }

  render() {
    let searchContentView;
    const { navigation } = this.props;
    if (this.state.searchState == "recent") {
      searchContentView = (
        <SearchRecent
          updateRecentState={this.updateRecentState}
          recentList={this.state.recentList}
          navigation={navigation}
        />
      );
    } else if (this.state.searchState == "suggestion") {
      searchContentView = (
        <SearchSuggestion
          onSearchText={this.state.onSearchText}
          updateSuggestionState={this.updateSuggestionState}
          suggestionList={this.state.suggestionList}
          navigation={navigation}
        />
      );
    } else if (this.state.searchState == "result") {
      searchContentView = (
        <SearchResult
          onSubmitSearchText={this.state.onSubmitSearchText}
          navigation={navigation}
        />
      );
    }
    return <View>{searchContentView}</View>;
  }

  suggest(value) {
    console.log(value);
  }
}

export default withNavigation(SearchScreen);
