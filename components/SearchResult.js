import React from "react";
import {
  Alert,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  ScrollView,
  AsyncStorage
} from "react-native";
import { Input } from "react-native-elements";
import { Ionicons } from "@expo/vector-icons";
import styles from "../styles/GlobalStyles";
import SearchHeaderNavigationBar from "./sub_components/SearchHeaderNavigationBar";
import afterBookmark from "../assets/images/ElementMenu/02BS-Bookmark.png";
import beforeBookmark from "../assets/images/ElementMenu/05BU-Bookmark.png";
import { withNavigation } from "react-navigation";
import NotFoundContent from "./sub_components/NotFoundContent";
import noResultFoundImage from "../assets/images/ElementMenu/Book.png";

class SearchResultScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nama: [],
      name: "",
      number: 0,
      resultList: [],
      bookmarkList: []
    };
  }

  componentDidMount() {
    this.setState({
      resultList: [],
      bookmarkList: []
    });

    AsyncStorage.getItem("bookmark").then(value => {
      this.setState({
        bookmarkList: JSON.parse(value) ? JSON.parse(value) : []
      });
    });

    this.fetchResultData();
  }

  async fetchResultData() {
    try {
      const response = await fetch(
        "http://192.168.1.80:9191/api/document/search/" +
          this.props.onSubmitSearchText,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json"
          }
        }
      );
      const responseJson = await response.json();
      for (let index = 0; index < responseJson.length; index++) {
        responseJson[index].bookmarked = false;
        this.state.bookmarkList.some(value => {
          if (value.personID == responseJson[index].personID) {
            responseJson[index].bookmarked = true;
          }
        });
      }
      console.log("Fetch Result List");
      console.log(responseJson);
      this.setState({
        resultList: responseJson
      });
      console.log("Update Result State");
      console.log(this.state.resultList);
    } catch (error) {
      console.log(
        "There has been a problem with your fetch operation: " + error.message
      );
      // ADD THIS THROW error
      throw error;
    }
  }

  saveBookmark = item => {
    AsyncStorage.getItem("bookmark").then(value => {
      const valueJson = JSON.parse(value) ? JSON.parse(value) : [];
      const list = [...valueJson, item];
      console.log("SAVED bookmark list");
      console.log(list);
      AsyncStorage.setItem("bookmark", JSON.stringify(list));
      // this.setState({
      //   bookmarkList: list
      // });
    });
  };

  removeBookmark = item => {
    AsyncStorage.getItem("bookmark").then(value => {
      const valueJson = JSON.parse(value) ? JSON.parse(value) : [];
      const list = [...valueJson];
      const removedIndex = list.findIndex(function(e) {
        return e.personID === item.personID;
      });
      if (removedIndex !== -1) {
        list.splice(removedIndex, 1);
      }
      AsyncStorage.setItem("bookmark", JSON.stringify(list));
      // this.setState({
      //   bookmarkList: list
      // });
    });
  };

  updateResultBookmarkStatus(index) {
    const newResultList = [...this.state.resultList];
    newResultList[index].bookmarked = !newResultList[index].bookmarked;
    this.setState({
      resultList: newResultList
    });
  }

  changeBookmarkState = index => {
    if (this.state.resultList[index].bookmarked) {
      this.removeBookmark(this.state.resultList[index]);
    } else {
      this.saveBookmark(this.state.resultList[index]);
    }
    this.updateResultBookmarkStatus(index);
  };

  renderBookmarkImage = bookmarkStatus => {
    // itemData.bookmarked = this.props.checkBookmarkData(itemData);
    var imgSource = bookmarkStatus ? afterBookmark : beforeBookmark;
    console.log("bookmark image status");
    return (
      <Image
        style={{ width: 20, height: 20, alignSelf: "flex-end" }}
        source={imgSource}
      />
    );
  };

  _OnButtonPress(newsID) {
    Alert.alert(newsID.toString());
  }

  render() {
    const textNotFound1 = "The item might be out in the space,";
    const textNotFound2 = "let's try another keyword";
    if (this.state.resultList.length == 0) {
      return (
        <NotFoundContent
          imageNotFound={noResultFoundImage}
          textNotFound1={textNotFound1}
          textNotFound2={textNotFound2}
          containerStyle={styles.searchNotFoundContainer}
          imageStyle={styles.noSearchResultImage}
        />
      );
    } else {
      return (
        <ScrollView style={styles.searchScrollViewStyle}>
          <FlatList
            data={this.state.resultList}
            renderItem={({ item, index }) => (
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("ProfileScreen", {
                    itemName: item.name,
                    itempid: item.personID,
                    itemUniversity: item.lastUniversity
                  });
                }}
              >
                <View key={item.personID} style={styles.resultContainer}>
                  <View style={{ flexDirection: "row" }}>
                    <Text numberOfLines={1} style={styles.resultNameText}>
                      {item.name}
                    </Text>
                    <TouchableOpacity
                      style={styles.searchTouchableOpacityImage}
                      onPress={() => this.changeBookmarkState(index)}
                    >
                      {this.renderBookmarkImage(
                        this.state.resultList[index].bookmarked
                      )}
                    </TouchableOpacity>
                  </View>
                  <Text numberOfLines={2} style={styles.resultUniversityText}>
                    Last Graduated at {item.lastUniversity}
                  </Text>
                  <Text style={styles.resultUniversityYearText}>
                    On {item.lastUniversityYear}
                  </Text>
                </View>
              </TouchableOpacity>
            )}
          />
        </ScrollView>
      );
    }
  }
}

export default withNavigation(SearchResultScreen);
