import React from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity
} from "react-native";
import { Input } from "react-native-elements";
import { Ionicons } from "@expo/vector-icons";
import SearchHeaderNavigationBar from "./sub_components/SearchHeaderNavigationBar";
import styles from "../styles/GlobalStyles";
import { withNavigation } from "react-navigation";

class SearchSuggestion extends React.Component {
  renderSeparator = () => (
    <View
      style={{
        backgroundColor: "black",
        height: 1
      }}
    />
  );

  render() {
    return (
      <View style={styles.searchRecentSuggestionContainer}>
        <Text style={styles.searchRecentSuggestionHeaderText}>Suggestion</Text>
        <Text style={styles.searchHorizontalLine}></Text>
        <FlatList
          ItemSeparatorComponent={this.renderSeparator}
          data={this.props.suggestionList}
          renderItem={({ item }) => (
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("ProfileScreen", {
                  itemName: item.name,
                  itempid: item.personID,
                  itemUniversity: item.lastUniversity
                })
              }
            >
              <View style={styles.searchRecentSuggestionTextContainer}>
                <Text
                  numberOfLines={1}
                  style={styles.searchRecentSuggestionName}
                >
                  {item.name}{" "}
                </Text>
                <Text style={styles.searchRecentSuggestionUniversity}>
                  {" "}
                  - Student at {item.lastUniversity}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    );
  }

  suggest(value) {
    console.log(value);
  }
}

export default withNavigation(SearchSuggestion);
