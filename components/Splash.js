import React from "react";
import { StyleSheet, Image } from "react-native";
import { LinearGradient } from "expo";
import styles from "../styles/GlobalStyles"

export default class SplashScreen extends React.Component {
  render() {
    return (
      <LinearGradient
        colors={["#7FC891", "#6DCBD9"]}
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <Image
          source={require("../assets/images/Logo/icon.png")}
          style={styles.logoIcon}
        />
      </LinearGradient>
    );
  }
}
