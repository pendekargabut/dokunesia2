import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  AsyncStorage
} from "react-native";
import { LinearGradient } from "expo";
import { FlatList } from "react-native-gesture-handler";
import styles from "../styles/GlobalStyles";
import NotFoundContent from "./sub_components/NotFoundContent";
import BookmarkList from "./sub_components/BookmarkList";
import afterBookmark from "../assets/images/ElementMenu/02BS-Bookmark.png";
import {
  NavigationActions,
  StackActions,
  withNavigation
} from "react-navigation";
import noBookmarkFoundImage from "../assets/images/ElementMenu/Book.png";

class BookmarkScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      bookmarkData: []
    };
    this.reloadPage = this.reloadPage.bind(this);
    // this.removeBookmark = this.removeBookmark.bind(this);
    // this.changeBookmarkState = this.changeBookmarkState.bind(this);
  }

  componentDidMount() {
    const { navigation } = this.props;
    this.focusListener = navigation.addListener("didFocus", () => {
      AsyncStorage.getItem("bookmark").then(value => {
        this.setState({
          bookmarkData: JSON.parse(value) ? JSON.parse(value) : []
        });
      });
    });
  }

  componentWillUnmount(){
    this.focusListener.remove();
  }

  reloadPage = () => {
    AsyncStorage.getItem("bookmark").then(value => {
      this.setState({
        bookmarkData: JSON.parse(value) ? JSON.parse(value) : []
      });
      console.log(this.state.bookmarkData);
    });
    console.log("Page Reloaded");
  };

  render() {
    let bookmarkContentView;
    const textNotFound1 = "You have no item you like yet,";
    const textNotFound2 = "Let's try so you don't have to searching again.";
    const { navigation } = this.props;
    if (this.state.bookmarkData.length == 0) {
      bookmarkContentView = (
        <NotFoundContent
          imageNotFound={noBookmarkFoundImage}
          textNotFound1={textNotFound1}
          textNotFound2={textNotFound2}
          containerStyle={styles.noBookmarkContainer}
          imageStyle={styles.noBookmarkImage}
        />
      );
    } else {
      bookmarkContentView = (
        <BookmarkList
          bookmarkData={this.state.bookmarkData}
          reloadPage={this.reloadPage}
          navigation={navigation}
        />
      );
    }

    return (
      <ScrollView>
        <View style={styles.halfScreenContainer}>
          <LinearGradient
            colors={["#7FC891", "#6DCBD9"]}
            style={styles.container}
          >
            <Image
              source={require("../assets/images/ElementMenu/09About.png")}
              style={styles.topHalfImageStyle}
              resizeMode="cover"
            />
            <View style={styles.topHalfTextContainer}>
              <Text style={styles.titleText}>Bookmark</Text>
              <Text style={styles.subTitleText}>
                Why must start over if you have Bookmark.
              </Text>
            </View>
          </LinearGradient>
        </View>
        {bookmarkContentView}
      </ScrollView>
    );
  }
}

export default withNavigation(BookmarkScreen);
