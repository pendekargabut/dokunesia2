import React from "react";
import { View } from "react-native";

export default class Carousel extends React.Component {
  render() {
    const { images } = this.props;
    if (images && images.length) {
      return <View style={styles.scrollContainer}></View>;
    }
    console.log("Please provide images");
    return null;
  }
}
