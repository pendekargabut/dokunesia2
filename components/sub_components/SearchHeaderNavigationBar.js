import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import { Input, Tile } from "react-native-elements";
import { Ionicons } from "@expo/vector-icons";
import SafeAreaView from "react-native-safe-area-view";
import styles from "../../styles/GlobalStyles";

class SearchHeaderNavigationBar extends React.Component {
  // onChangeTextState(text) {
  //   if (text.length == 0) {
  //     this.props.changeSearchState("recent");
  //   } else {
  //     this.props.changeSearchState("suggestion");
  //     this.props.updateOnChangeText(text);
  //   }
  // }

  render() {
    return (
      <SafeAreaView
        forceInset={{ top: "always" }}
        style={{
          backgroundColor: "#6DCBD9"
        }}
      >
        <View
          style={{ flexDirection: "row", marginHorizontal: 10, marginTop: 5 }}
        >
          <TouchableOpacity
            style={{
              marginTop: 8,
              height: "100%",
              width: "15%",
              alignSelf: "flex-start"
            }}
            onPress={() => this.props.navigation.pop()}
          >
            <Image
              source={require("../../assets/images/ElementMenu/Arrow.png")}
              style={{ alignSelf: "flex-start", justifyContent: "center" }}
            />
          </TouchableOpacity>

          <Text style={styles.getStartedTextsearch5}>{this.props.title}</Text>
        </View>
        <View style={{ flexDirection: "row", marginVertical: 5 }}>
          <Input
            leftIcon={{ type: "search", name: "search" }}
            placeholder="Names, ID, Card Number..."
            clearButtonMode={"while-editing"}
            returnKeyType={"search"}
            onChangeText={text => {
              if (text.length == 0) {
                this.props.onRecentState("recent");
              } else {
                // this.props.updateOnChangeText(text);
                // this.props.changeSearchState("suggestion");
                this.props.onSuggestionChange(text, "suggestion");
              }
            }}
            onSubmitEditing={event => {
              this.props.onSubmitState(event.nativeEvent.text, "result");
            }}
            containerStyle={{
              backgroundColor: "#FFFFFF00",
              borderWidth: 0, //no effect
              shadowColor: "white", //no effect
              borderBottomColor: "transparent",
              borderTopColor: "transparent",
              width: "100%"
            }}
            inputContainerStyle={{
              backgroundColor: "#FFFFFF",
              //shadowColor: 'black',
              borderWidth: 1,
              borderRadius: 2,
              borderColor: "#ddd",
              borderBottomWidth: 0,
              shadowColor: "#000",
              shadowOffset: { width: 100, height: 200 },

              shadowOpacity: 20.0,
              shadowRadius: 200,
              elevation: 1
              // marginLeft: 5,
              // marginRight: 5,
              // marginTop: 10,
            }}
          />
        </View>
      </SafeAreaView>
    );
  }
}

export default SearchHeaderNavigationBar;
