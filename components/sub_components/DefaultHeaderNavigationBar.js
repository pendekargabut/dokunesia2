import React from "react";
import { View, Image, Text, StyleSheet, TouchableOpacity } from "react-native";
import SafeAreaView from "react-native-safe-area-view";
import styles from "../../styles/GlobalStyles";
import { NavigationActions, StackActions } from "react-navigation";

const DefaultHeaderNavigationBar = props => {
  return (
    <SafeAreaView
      forceInset={{ top: "always" }}
      style={{
        backgroundColor: "#6DCBD9"
      }}
    >
      <View
        style={{
          flexDirection: "row",
          margin: 10,
          width: "100%"
        }}
      >
        <TouchableOpacity
          style={{
            marginTop: 8,
            height: "100%",
            width: "15%",
            alignSelf: "flex-start"
          }}
          onPress={() => props.navigation.goBack()}
        >
          <Image
            source={require("../../assets/images/ElementMenu/Arrow.png")}
            style={{ alignSelf: "flex-start", justifyContent: "center" }}
          />
        </TouchableOpacity>
        <Text numberOfLines={1} style={styles.getStartedTextsearch5}>
          {props.title}
        </Text>
      </View>
    </SafeAreaView>
  );
};

export default DefaultHeaderNavigationBar;
