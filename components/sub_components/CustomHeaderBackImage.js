import React from "react";
import { Image, StyleSheet } from "react-native";

export default class CustomHeaderBackImage extends React.Component {
  render() {
    return (
      <Image
        source={require("../../assets/images/ElementMenu/Arrow.png")}
        style={styles.header}
      />
    );
  }
}

const styles = StyleSheet.create({
  header: {
    textAlign: "left",
    marginTop: 3
  }
});
