import React from "react";
import { View, Text, Image } from "react-native";
import styles from "../../styles/GlobalStyles";

const NotFoundContent = props => {
  return (
    <View style={props.containerStyle}>
      <Image
        source={props.imageNotFound}
        style={props.imageStyle}
        resizeMode="contain"
      />
      <Text style={styles.notFoundText}>{props.textNotFound1}</Text>
      <Text style={styles.notFoundText}>{props.textNotFound2}</Text>
    </View>
  );
};

export default NotFoundContent;
