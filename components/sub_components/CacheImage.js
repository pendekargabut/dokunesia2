import React from "react";
import { ScrollView, Image, TouchableOpacity, Dimensions, View, Modal, TouchableWithoutFeedback } from "react-native";
import { FileSystem } from "expo";
import shorthash from "shorthash";
import ImageViewer from 'react-native-image-zoom-viewer';
import ImageView from "react-native-image-view";
import ImageZoom from 'react-native-image-pan-zoom';
import { ThemeProvider } from "react-native-elements";
import { SliderBox } from 'react-native-image-slider-box';
import { Ionicons } from "@expo/vector-icons";
const screenWidth = Math.round(Dimensions.get("window").width);
export default class CacheImage extends React.Component {
  state = {
    images: [],
    isVisible: false,
    imagesview: [],
    imagecarousel: [],
    isModelVisible: false
  };

  componentDidMount = async () => {

    const { uri } = this.props;
    const { length } = this.props;
    for (let a of uri) {
      // console.log("bnayak"+length);
      // console.log(uri[i]);
      console.log(a);

      const name = shorthash.unique(a);
      console.log(name);
      const path = `${FileSystem.cacheDirectory}${name}`;
      console.log(path);
      const image = await FileSystem.getInfoAsync(path);
      if (image.exists) {
        //console.log(image.uri)
        var newImage = {
          key: name,
          source: {
            uri: image.uri
          }
        };
        var newImage2 = image.uri;

        var oldImageArr = this.state.images;
        var newImageArr = [newImage, ...oldImageArr];

        var oldImageArr2 = this.state.imagecarousel;
        var newImageArr2 = [newImage2, ...oldImageArr2];

        this.setState(
          {
            images: newImageArr, imagecarousel: newImageArr2
          },
          () => {
            console.log(this.state);
          }
        );

        //     var newarray = this.state.images.push(
        //       {  source: {
        //           uri: image.uri,
        //         }
        //     });
        //   //  console.log(newarray);
        //     console.log('read image from cache');
        //     console.log(this.state.images);
        //  //   this.setState(images=newarray);
        // console.log(this.state.images);

        // return;
      } else {
        console.log("downloading image to cache " + a);
        const newImage = await FileSystem.downloadAsync(a, path).then(res => {
          console.log("sudah");
          var newImage = {
            key: name,
            source: {
              uri: a
            }
          };
          var newImage2 = a;

          var oldImageArr = this.state.images;
          var newImageArr = [newImage, ...oldImageArr];

          var oldImageArr2 = this.state.imagecarousel;
          var newImageArr2 = [newImage2, ...oldImageArr2];

          this.setState(
            {
              images: newImageArr, imagecarousel: newImageArr2
            },
            () => {
              console.log(this.state);
            }
          );

          // this.setState(this.state.images.push({
          //   key: i,
          //   source: {
          //     uri: image.uri,
          //   },
          // }))
        });
      }
    }
  };

  imageView = (image) => {

    images = [
      {
        source: {
          uri: image,
        },
        // title: 'Paris',
        width: screenWidth,
        height: 300,
      },
    ];
    this.setState({ imagesview: images, isVisible: true })
    //this.setState({})
    // console.log(images)
    //     return (



    //         <ImageView
    //           images={images}
    //           // source={{ uri: itemId }}
    //           //  imageWidth={800}
    //           //  imageHeight={800}
    //           imageIndex={0}
    //           isVisible={true}
    //         // renderFooter={(currentImage) => (<View><Text>My footer</Text></View>)}
    //         />




    //     );
  }
  ShowModalFunction(visible) {
    this.setState({ isModelVisible: false });
  }
  render() {
    var img = [];
    var images = [
      {
        source: {
          uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRGP8pv6U1_Defho1ww1pprKscLOE5OGWt1XjmvxPQK_vgA3IcT',
        },
        // title: 'Paris',
        width: screenWidth,
        height: 300,
      },
    ];
    // for (z = 0; z < this.state.images.length; z++) {

    //   img.push(<Image style={styles.image} source={this.state.images[z].source} key={z} />);
    // }
    //   console.log("render "+this.state.images);
    //  console.log(img)
    //return <Image style={this.props.style} source={this.state.images[0].source} />;

    return (
      <View>
        {this.state.imagecarousel !== '' ?
          <SliderBox
            images={this.state.imagecarousel}
            sliderBoxHeight={300}
            onCurrentImagePressed={index => {
              var imgview = [{ url: this.state.imagecarousel[index] }]
              this.setState({ imagesview: imgview, isModelVisible: true })
            }

            }
          /> : null}


        <Modal
          visible={this.state.isModelVisible}
          transparent={false}
          onRequestClose={() => this.ShowModalFunction()}>
          <View style={{ backgroundColor: "black" }}>
            <TouchableWithoutFeedback
              onPress={() => {
                this.setState({ isModelVisible: false })
              }}>


              <Ionicons
                style={{ padding: 8 }}
                color="white"
                size={35}
                name="ios-close-circle-outline"
              />

            </TouchableWithoutFeedback>
          </View>
          <ImageViewer imageUrls={this.state.imagesview} />
        </Modal>


      </View>
      // <ScrollView
      //   horizontal
      //   pagingEnabled
      //   showsHorizontalScrollIndicator={
      //     false
      //   }
      // //  key='test'
      // >

      //  {
      //           this.state.images.map(image => (
      //           // <TouchableOpacity
      //           //   onPress={() => { 
      //           //     this.imageView(image.source.uri) 
      //           //    // this.props.navigation.navigate('ImageView',{pict:image.source.uri})


      //           //     }}>
      //           //   <Image
      //           //     style={this.props.style}
      //           //     source={image.source}
      //           //     key={image.key}
      //           //   />
      //           // </TouchableOpacity>
      //           <ImageZoom cropWidth={screenWidth}
      //           cropHeight={300}
      //           imageWidth={screenWidth}
      //           imageHeight={200}>
      //    <Image style={this.props.style}
      //            source={image.source}/>
      // </ImageZoom>
      //         ))

      //}

      // {/* <Image style={this.props.style} source={this.state.images[0].source} />
      //     <Image style={this.props.style} source={this.state.images[1].source} /> */}
      //  {/* {img} */}
      // {/* <ImageView
      //   images={this.state.imagesview}
      //   // source={{ uri: itemId }}
      //   //  imageWidth={800}
      //   //  imageHeight={800}
      //   imageIndex={0}
      //   isVisible={this.state.isVisible}
      // // renderFooter={(currentImage) => (<View><Text>My footer</Text></View>)}
      // /> */}

      // {/* </ScrollView> */}

    );
  }
}
