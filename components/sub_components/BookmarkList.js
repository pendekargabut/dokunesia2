import React from "react";
import {
  View,
  Text,
  AsyncStorage,
  FlatList,
  Image,
  TouchableOpacity
} from "react-native";
import styles from "../../styles/GlobalStyles";
import afterBookmark from "../../assets/images/ElementMenu/02BS-Bookmark.png";

class BookmarkList extends React.Component {
  removeBookmark = itemObj => {
    AsyncStorage.getItem("bookmark").then(value => {
      const list = [...JSON.parse(value)];
      const removedIndex = list.findIndex(function(e) {
        return e.personID === itemObj.personID;
      });
      console.log(removedIndex);
      if (removedIndex !== -1) {
        list.splice(removedIndex, 1);
      }
      console.log(list);
      AsyncStorage.setItem("bookmark", JSON.stringify(list));
      this.props.reloadPage();
    });
  };

  changeBookmarkState = (dataSource, index) => {
    console.log(dataSource)
    const bookmarkedValue = [...dataSource];
    if (bookmarkedValue[index].bookmarked) {
      console.log("remove bookmark called")
      this.removeBookmark(bookmarkedValue[index]);
    }
    //   AsyncStorage.getItem("bookmark").then(value => {
    //     this.setState({
    //       bookmarkData: JSON.parse(value)
    //     });
    //   });
  };

  render() {
    return (
      <View style={styles.bookmarkContentContainer}>
        <FlatList
          style={styles.bookmarkFlatList}
          data={this.props.bookmarkData}
          renderItem={({ item, index }) => (
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("ProfileScreen", {
                  itemName: item.name,
                  itempid: item.personID,
                  itemUniversity: item.lastUniversity
                })
              }
            >
              <View key={item.personID} style={styles.resultContainer}>
                <View style={{ flexDirection: "row" }}>
                  <Text numberOfLines={1} style={styles.resultNameText}>
                    {item.name}
                  </Text>
                  <TouchableOpacity
                    style={{
                      marginTop: 10,
                      width: 30,
                      height: 30,
                      alignSelf: "center"
                    }}
                    onPress={() => {
                      this.changeBookmarkState(this.props.bookmarkData, index);
                    }}
                  >
                    <Image
                      style={{ width: 20, height: 20, alignSelf: "flex-end" }}
                      source={afterBookmark}
                    />
                  </TouchableOpacity>
                </View>
                <Text numberOfLines={2} style={styles.resultUniversityText}>
                  Last Graduated at {item.lastUniversity}
                </Text>
                <Text style={styles.resultUniversityYearText}>
                  On {item.lastUniversityYear}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    );
  }
}

export default BookmarkList;
