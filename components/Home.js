import React from "react";
import { StyleSheet, Text, View, ImageBackground } from "react-native";
import { SearchBar } from "react-native-elements";
import styles from "../styles/GlobalStyles"

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null
  }
  render() {
    return (
      <View style={styles.container}>
        <ImageBackground
          resizeMode={"stretch"}
          source={require("../assets/images/ElementMenu/11BackgroundHome.png")}
          style={styles.backgroundImage}
        >
          <View style={styles.homeView}>
            <Text style={styles.getStartedText2}>Hello,</Text>

            <Text style={styles.getStartedText}>Find your degrees here.</Text>
            <SearchBar
              placeholder="Names, ID, Card Number..."
              onFocus={() => this.props.navigation.push("SearchScreen")}
              containerStyle={styles.searchBarContainer}
              inputContainerStyle={styles.searchBarInputContainer}
            />
          </View>
        </ImageBackground>
      </View>
    );
  }
}