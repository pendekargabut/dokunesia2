import React from "react";
import { StyleSheet, Dimensions } from "react-native";

const screenHeight = Math.round(Dimensions.get("window").height);
const screenWidth = Math.round(Dimensions.get("window").width);

const globalStyles = StyleSheet.create({
  logoIcon: {
    flex: 1,
    resizeMode: "center"
  },
  container: {
    flex: 1
  },
  logoIcon: {
    flex: 1,
    resizeMode: "center"
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "stretch"
  },
  homeView: {
    position: "absolute",
    top: 100,
    bottom: 0,
    left: 10,
    right: 10
  },
  searchBarContainer: {
    backgroundColor: "#FFFFFF00",
    borderWidth: 0, //no effect
    shadowColor: "white", //no effect
    borderBottomColor: "transparent",
    borderTopColor: "transparent"
  },
  searchBarInputContainer: {
    backgroundColor: "#FFFFFF",
    //shadowColor: 'black',
    borderWidth: 1,
    borderRadius: 2,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: { width: 100, height: 200 },
    shadowOpacity: 20.0,
    shadowRadius: 200,
    elevation: 1
    // marginLeft: 5,
    // marginRight: 5,
    // marginTop: 10,
  },
  viewheader: {
    backgroundColor: "#04d1bd",
    height: 100
  },
  viewheaderprofile: {
    backgroundColor: "#04d1bd",
    height: 70
  },
  viewheadersearch: {
    backgroundColor: "#04d1bd",
    height: 60,
    flexDirection: "row"
  },
  viewheadersearch2: {
    backgroundColor: "#04d1bd",
    height: 90,
    flexDirection: "row"
  },

  getStartedText: {
    fontSize: 20,
    color: "rgba(255,255,255, 1)",
    lineHeight: 25,
    textAlign: "left",
    marginLeft: 8
  },
  getStartedTextsearch: {
    paddingTop: 35,
    fontSize: 25,
    color: "rgba(255,255,255, 1)",
    lineHeight: 25,
    textAlign: "left",
    marginLeft: 8
  },
  getStartedTextsearch2: {
    paddingTop: 39,
    //fontSize: 20,
    color: "rgba(255,255,255, 1)",
    lineHeight: 25,
    textAlign: "left",
    marginLeft: 8
  },
  getStartedTextsearch3: {
    //fontSize: 20,
    color: "rgba(255,255,255, 1)",
    lineHeight: 45,
    textAlign: "left",
    marginLeft: 8
  },
  getStartedTextsearch4: {
    paddingTop: 52,
    //fontSize: 20,
    color: "rgba(255,255,255, 1)",
    lineHeight: 25,
    textAlign: "left",
    marginLeft: 8
  },
  getStartedTextsearch5: {
    fontSize: 25,
    color: "rgba(255,255,255, 1)",
    fontWeight: "bold",
    marginRight: 15,
    paddingRight: 10,
    width: "84%"
  },
  getStartedTextprofile: {
    paddingTop: 35,
    fontSize: 30,
    color: "rgba(255,255,255, 1)",
    lineHeight: 40,
    textAlign: "left",
    justifyContent: "center",
    marginLeft: 8
  },
  getStartedText2: {
    fontSize: 20,
    fontWeight: "bold",
    color: "rgba(255,255,255, 1)",
    lineHeight: 25,
    textAlign: "left",
    marginLeft: 8
  },
  resultContainer: {
    borderRadius: 8,
    flex: 1,
    backgroundColor: "white",
    margin: 10,
    marginTop: 10,
    marginBottom: 10,
    borderWidth: 1,
    borderColor: "lightgrey",
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
    height: 130
  },
  resultNameText: {
    fontSize: 18,
    fontWeight: "bold",
    paddingHorizontal: 10,
    paddingTop: 10,
    paddingBottom: 5,
    width: "90%",
    alignSelf: "center",
    justifyContent: "flex-start",
    alignContent: "center"
  },
  resultUniversityText: {
    paddingHorizontal: 10
  },
  resultUniversityYearText: {
    position: "absolute",
    left: 10,
    bottom: 15
  },
  topHalfTextContainer: {
    position: "absolute",
    top: 50,
    left: 20,
    right: 20
  },
  topHalfImageStyle: {
    position: "absolute",
    right: 0,
    bottom: 0,
    height: "80%",
    width: "80%"
  },
  halfScreenContainer: {
    height: screenHeight / 2,
    width: "100%"
  },
  titleText: {
    color: "#ffffff",
    fontWeight: "bold",
    fontSize: 25,
    backgroundColor: "rgba(52, 52, 52, 0)"
  },
  subTitleText: {
    color: "#ffffff",
    fontSize: 22,
    backgroundColor: "rgba(52, 52, 52, 0)"
  },
  notFoundText: {
    fontWeight: "bold",
    textAlign: "center"
  },

  // Search Style
  searchRecentSuggestionContainer: {
    marginLeft: 20,
    marginTop: 5
  },
  searchRecentSuggestionHeaderText: {
    fontSize: 25,
    fontWeight: "bold",
    paddingBottom: 5
  },
  searchHorizontalLine: {
    backgroundColor: "black",
    height: 1
  },
  searchRecentSuggestionTextContainer: {
    flexDirection: "row",
    padding: 10,
    paddingLeft: 0
  },
  searchRecentSuggestionName: {
    fontSize: 16
  },
  searchRecentSuggestionUniversity: {
    fontSize: 16,
    color: "grey"
  },
  searchScrollViewStyle: {
    paddingLeft: 8,
    paddingRight: 8
  },
  searchTouchableOpacityImage: {
    marginTop: 10,
    width: 30,
    height: 30,
    alignSelf: "center"
  },
  searchNotFoundContainer: {
    width: "100%",
    height: screenHeight - 200,
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center"
  },
  noSearchResultImage: {
    width: "50%",
    height: "50%",
    alignSelf: "center"
  },


  //Profile Style
  profileTextContainer: {
    marginBottom: 30
  },
  profileTextHeaderStyle: {
    fontSize: 20,
    fontWeight: "bold",
    paddingTop: 10,
    paddingBottom: 20
  },
  profileCacheImageStyle: {
    width: screenWidth - 16,
    height: 300,
    marginBottom: 30
  },
  profileTextContainer: {
    flexDirection: "row"
  },
  profileTextStyle: {
    minWidth: 120
  },

  //Bookmark Style
  bookmarkContentContainer: {
    flex: 1,
    // width: "100%",
    justifyContent: "center",
    alignContent: "center",
    alignSelf: "stretch"
  },
  bookmarkFlatList: {
    paddingTop: 10,
    marginBottom: 10
  },
  bookmarkViewContent: {
    borderRadius: 8,
    flex: 1,
    backgroundColor: "white",
    margin: 10,
    marginTop: 10,
    marginBottom: 15,
    padding: 10,
    borderWidth: 1,
    borderColor: "lightgrey",
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
    height: 150
  },
  noBookmarkContainer: {
    height: screenHeight / 2.5,
    width: "100%",
    justifyContent: "center",
    alignContent: "center"
  },
  noBookmarkImage: {
    width: "70%",
    height: "70%",
    alignSelf: "center"
  },

  //About Style
  aboutContentContainer: {
    flex: 1,
    height: 460,
    justifyContent: "space-around"
  },
  aboutContentWrapper: {
    flexDirection: "row",
    justifyContent: "space-around"
  },
  aboutContentRightText: {
    flex: 0.45,
    marginRight: 10,
    justifyContent: "flex-end"
  },
  aboutContentLeftText: {
    flex: 0.45,
    marginLeft: 10,
    justifyContent: "flex-start"
  },
  aboutContentTitleTextStyle: {
    fontWeight: "bold",
    marginBottom: 10
  },
  aboutContentImage: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start"
  }
});

export default globalStyles;
